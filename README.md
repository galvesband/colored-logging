# Introducción #

Implementa clases que permiten añadir colores e información del modelo identificado
en los logs en archivos de texto de un proyecto Laravel. Los archivos de log son un poco 
menos útiles al revisarlos en un editor de texto al incluir caracteres de formato al 
principio y al final de los mensajes para modificar los colores de frente y fondo, pero 
son notablemente más fáciles de seguir al visualizarlos en consolas con soporte para ello.

El formateador también incluye información del usuario como parte de la cabecera del log,
para permitir saber de un vistazo qué modelo estaba identificado cuando se generó el log. 

Basado en la implementación original en Beat/Logistics, que a su vez utiliza 
`bramus/monolog-colored-line-formatter`. 


# Instalación #

El paquete no está alojado en repositorios de paquetes públicos 
como Packagist, por lo que es necesario configurar en el `composer.json`
del proyecto en el que lo quieras usar los datos para acceder al repositorio
con el código del paquete.


## Cambios a `composer.json` del proyecto en el que se va a instalar ##

Ante la duda, seguir las instrucciones descritas en [la documentación
de Composer](https://getcomposer.org/doc/05-repositories.md#using-private-repositories).

A grandes rasgos, hay que añadir a `composer.json` la siguiente información, sustituyendo
la versión por una cadena apropiada:

```
{
  [...],
  "repositories": [
    "type": "vcs",
    "url": "git@10.45.5.2:beat/colored-logging"
  ],
  "require": {
    [...],
    "beat/colored-logging": "<tag or branch name or hash>"
  }
  [...]
}
```

En el campo "url" de la sección de repositorios debe figurar una cadena que permita conectar con el repositorio de
código. La máquina que ejecute composer debe tener instalado GIT y SSH y la configuración de SSH debe permitir
conectar con la máquina que aloja el repositorio y leer el mismo. **Esto es muy importante**, o composer no será 
capaz de instalar las dependencias. 

Una vez actualizado `composer.json`, habrá que ejecutar composer para que descargue 
la nueva dependencia: `composer update`.


### Sobre el acceso al repositorio ###

El SSH que use composer para descargar los repositorios debe tener una configuración similar a la siguiente
en su archivo de configuración (en Linux suele ser `$HOME/.ssh/config`). Por supuesto, ese mismo SSH
debe tener las llaves de acceso para llegar al repositorio (la de Beatlab y, si es desde fuera de la nave,
la de Taycan).

```
# Solo necesario si se ataca desde fuera de la nave.
Host taycan
        Hostname 88.26.234.124
        Port 18822
        User beat
        IdentityFile ~/.ssh/id_taycan
        IdentitiesOnly yes

Host 10.45.5.2
        Port 2224
        User git
        IdentitiesOnly yes
        IdentityFile ~/.ssh/id_beatlab
        # Comentar esta línea si estamos fuera de la nave.
        ProxyJump taycan
```


# Configuración #

El paquete no requiere de ServiceProvider actualmente (aunque puede que en el futuro añadamos uno que
permita configurar algunos parámetros del formato de los logs). Sí es necesario modificar la configuración
de logs del proyecto Laravel para incluir nuestro formateador en Monolog


## Configurar formateador ##

En el archivo `logging.php`, hay que añadir el _Tap_ `CustomColoredLogTap` a los archivos de log configurados. 
Por ejemplo:

```php
// Extracto de config/logging.php

// ...

  'channels' => [
    'main' => [
      'driver'     => 'daily',
      'path'       => storage_path('logs/main/main.log'),
      'level'      => 'debug',
      'days'       => 14,
      'permission' => 0777,
      'tap'        => [CustomColoredLogTap::class],
    ],
  ],

// ...

```

Por defecto, el formateador se limita a poner líneas de colores, sin modificaciones al contenido de los logs.
Se pueden configurar los modificadores aplicados definiendo la siguiente configuración, tambien en `logging.php`:

```php
  'record_mutators' => [
    \Beat\ColoredLogging\RecordMutators\InternalTrim::class,
    \Beat\ColoredLogging\RecordMutators\AddAuthInfo::class,
  ],
```

Cada modificador hace _algo_ al mensaje de log. Quita y pon los que sean apropiados. Algunos requieren de pasos
adicionales.

A continuación se describe cada uno de los modificadores existentes.


## AddAuthInfo ##

Este modificador añade información del usuario identificado al mensaje de log. Por defecto, sin otras modificaciones,
añadirá a la cabecera del log el nombre de la clase y el identificador de la misma.

```
[2023-08-03 11:23:42] testing.Usuario#123.INFO: Something happened!
```

Es posible personalizar el contenido de la nueva sección del mensaje de log si los modelos que son identificados
implementan la interfaz `LoggableAuthenticatable`. Por ejemplo:

```php
use \Beat\ColoredLogging\Contracts\LoggableAuthenticatable;

class Usuario extends Authenticatable implements LoggableAuthenticatable
{
  // [...]
  
  public function loggingString(): string
  {
    // Incluir aquí cualquier información relevante, como Almacén, tipo de identificación, ...
    return $this->alias;
  }
}
```

Por ejemplo, en un sistema de Logística como _Beat Logistics_ la interfaz sería implementada por 
la clase `Operario` y podría devolver una cadena que incluyera información como:

 - El alias del operario
 - El almacén del operario
 - Si el operario es simulado, de sistema o normal
 - Qué tipo de autenticación se utilizó (Token, JWT, IP, ...)
 - Entorno configurado
 - Empresa configurada


## InternalTrim ##

Este es un modificador simple que no requiere de personalización. Elimina dobles espacios del mensaje de log.