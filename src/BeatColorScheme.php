<?php

namespace Beat\ColoredLogging;

use Bramus\Monolog\Formatter\ColorSchemes\ColorSchemeInterface;
use Bramus\Monolog\Formatter\ColorSchemes\ColorSchemeTrait;
use Monolog\Logger;
use Bramus\Ansi\ControlSequences\EscapeSequences\Enums\SGR;
use Throwable;

/**
 * Esquema de colores normalmente utilizado en proyectos BEAT.
 */
class BeatColorScheme implements ColorSchemeInterface
{
    use ColorSchemeTrait {
        ColorSchemeTrait::__construct as private __constructTrait;
    }

    /**
     * Constructor
     * @throws Throwable
     */
    public function __construct ()
    {
        $this->__constructTrait();
        $this->setColorizeArray(array(
            Logger::DEBUG     => $this->ansi->color(SGR::COLOR_FG_WHITE)->get(),
            Logger::INFO      => $this->ansi->color(SGR::COLOR_FG_GREEN)->get(),
            Logger::WARNING   => $this->ansi->color(SGR::COLOR_FG_YELLOW)->get(),
            Logger::ERROR     => $this->ansi->color(SGR::COLOR_FG_RED)->get(),
            Logger::NOTICE    => $this->ansi->color(SGR::COLOR_FG_CYAN)->get(),
            Logger::CRITICAL  => $this->ansi->color(SGR::COLOR_FG_RED)->underline()->get(),
            Logger::ALERT     => $this->ansi->color(array(SGR::COLOR_FG_YELLOW_BRIGHT, SGR::COLOR_BG_RED_BRIGHT))->get(),
            Logger::EMERGENCY => $this->ansi->color(SGR::COLOR_BG_RED_BRIGHT)->blink()->color(SGR::COLOR_FG_YELLOW_BRIGHT)->get()
        ));
    }
}
