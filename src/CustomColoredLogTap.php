<?php

namespace Beat\ColoredLogging;

use Beat\ColoredLogging\Contracts\RecordMutator;
use Illuminate\Log\Logger;

/**
 * Colorea mensajes de logs y aplica una lista de modificadores.
 *
 * La lista de modificadores es leída de la clave `record_mutators`
 * del archivo de configuración `logging.php` de Laravel.
 */
class CustomColoredLogTap
{
    public function __invoke (Logger $logger)
    {
        /** @noinspection PhpUndefinedMethodInspection */
        foreach ($logger->getHandlers() as $handler) {

            // Establecemos nuestro formateador personalizado en todos los handlers.
            $handler->setFormatter(

                new CustomColoredLineFormatter(
                    new BeatColorScheme(),
                    null,
                    null,
                    true,
                    true,
                    $this->get_mutators()
                )

            );

        }
    }

    /**
     * Devuelve instancias de los modificadores a aplicar a los mensajes.
     *
     * Las instancias serán generadas por el inyector de dependencias a partir
     * de la lista de clases definida en el archivo de configuración `logging.php`.
     *
     * @return RecordMutator[]
     */
    protected function get_mutators(): array
    {
        $class_list = config('logging.record_mutators', []);

        return array_map(function (string $class_name) {
            return app($class_name);
        }, $class_list);
    }
}
