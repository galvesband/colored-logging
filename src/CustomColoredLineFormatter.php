<?php

namespace Beat\ColoredLogging;

use Beat\ColoredLogging\Contracts\LoggableAuthenticatable;
use Beat\ColoredLogging\Contracts\RecordMutator;
use Beat\ColoredLogging\RecordMutators\AddAuthInfo;
use Beat\ColoredLogging\RecordMutators\InternalTrim;
use Bramus\Monolog\Formatter\ColoredLineFormatter;
use Bramus\Monolog\Formatter\ColorSchemes\ColorSchemeInterface;
use Illuminate\Contracts\Auth\Authenticatable;
use Illuminate\Support\Facades\Auth;
use Throwable;

/**
 * Formateador que extiende ColoredLineFormatter y aplica una lista de modificaciones a los mensajes de log.
 *
 * Los mensajes son modificados por los RecordMutator proporcionados al construir el formateador.
 */
class CustomColoredLineFormatter extends ColoredLineFormatter
{
    protected $maxNormalizeDepth = 15;

    /** @var RecordMutator[] */
    private array $mutators;

    /**
     * @param ColorSchemeInterface|null $color_scheme
     * @param string|null               $format
     * @param string|null               $date_format
     * @param bool                      $allow_inline_line_breaks
     * @param bool                      $ignore_empty_context
     * @param RecordMutator[]           $mutators
     */
    public function __construct (
        ?ColorSchemeInterface $color_scheme             = null,
        ?string               $format                   = null,
        ?string               $date_format              = null,
        bool                  $allow_inline_line_breaks = false,
        bool                  $ignore_empty_context     = false,
        array                 $mutators                 = []
    ) {
        $date_format = $date_format ?? 'Y-m-d H:i:s';
        parent::__construct($color_scheme, $format, $date_format, $allow_inline_line_breaks, $ignore_empty_context);
        $this->mutators = $mutators;
    }

    /**
     * Método que formatea el mensaje de log.
     *
     * @param  array $record
     * @return string
     * @throws Throwable
     */
    public function format (array $record) : string
    {
        // Aplicamos los mutators a record.
        // Nos tragamos silenciosamente errores.
        try {
            foreach ($this->mutators as $mutator) {
                $record = $mutator->mutateRecord($record);
            }
        } catch (Throwable $t) {}

        // Si hay un formato personalizado para este mensaje, lo utilizamos.
        // En otro caso, dejamos el que teníamos configurado.
        // Pase lo que pase, restauramos el original.
        try {
            $original_format = $this->format;
            $this->format    = $record['custom_format'] ?? $this->format;

            return parent::format($record);
        } finally {
            $this->format    = $original_format;
        }
    }
}
