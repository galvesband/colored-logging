<?php

namespace Beat\ColoredLogging\Contracts;

interface RecordMutator
{
    public function mutateRecord(array $record): array;
}