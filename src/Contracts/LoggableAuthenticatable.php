<?php

namespace Beat\ColoredLogging\Contracts;

use Illuminate\Contracts\Auth\Authenticatable;

interface LoggableAuthenticatable extends Authenticatable
{
    /**
     * Cadena a utilizar para identificar al usuario en logs.
     *
     * @return string
     */
    public function loggingString(): string;
}
