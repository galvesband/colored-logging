<?php

namespace Beat\ColoredLogging\RecordMutators;

use Beat\ColoredLogging\Contracts\LoggableAuthenticatable;
use Beat\ColoredLogging\Contracts\RecordMutator;
use Illuminate\Contracts\Auth\Authenticatable;
use Illuminate\Support\Facades\Auth;
use Throwable;

class AddAuthInfo implements RecordMutator
{
    public const LOG_FORMAT = '[%datetime%] %channel%.%auth%.%level_name%: %message% %context% %extra%\n';

    public function mutateRecord(array $record): array
    {
        $record['auth']          = $this->resolve_authenticatable_string();
        $record['custom_format'] = static::LOG_FORMAT;

        return $record;
    }

    /**
     * Devuelve una cadena que represente al usuario o modelo identificado, o '?'.
     *
     * Si el modelo identificado implementa LoggableAuthenticatable, entonces la cadena devuelta
     * será el resultado de llamar al método loggingString() del modelo. En otro caso,
     * devolverá el resultado de llamar al método propio getDefaultAuthenticatableString().
     *
     * @return string
     * @see LoggableAuthenticatable
     * @see self::get_default_authenticatable_string()
     */
    protected function resolve_authenticatable_string(): string
    {
        // Identificamos al usuario. Si no hay usuario identificado, paramos.
        /** @var LoggableAuthenticatable|Authenticatable|null $authenticatable */
        $authenticatable = Auth::user();
        if (!$authenticatable) {
            return "?";
        }

        // Hay algo identificado. Si implementa nuestro contrato especial, le podemos pedir que
        // genere él solo la cadena que hay que incluir en el log.
        // Lo englobamos en un try-catch porque queremos asegurarnos de que no petamos malamente.
        if ($this->object_implements_interface($authenticatable, LoggableAuthenticatable::class)) {
            try {
                return $authenticatable->loggingString();
            } catch (Throwable $t) {
                $id = $authenticatable->getAuthIdentifier();
                return "log_error[{$id}]({$t->getMessage()})";
            }
        }

        // Si hemos llegado aquí sin retornar, usamos algo de Authenticatable, que debe de estar implementado
        // seguro si el modelo es el que figura como identificado por Laravel.
        return $this->get_default_authenticatable_string($authenticatable);
    }

    /**
     * Genera una cadena apropiada para logs que identifica de forma única al modelo identificado usando
     * solo símbolos de Authenticatable de Laravel.
     *
     * Por ejemplo, si un modelo "App\Models\Usuario" con identificador 42 no implementa LoggableAuthenticatable
     * y está identificado durante la generación de un mensaje de log, la parte del log que
     * lo identifica será "Usuario#42".
     *
     * @param Authenticatable $authenticatable
     * @return string
     */
    protected function get_default_authenticatable_string(Authenticatable $authenticatable): string
    {
        $auth_class = basename(str_replace('\\', '/', get_class($authenticatable)));
        return "{$auth_class}#{$authenticatable->getAuthIdentifier()}";
    }

    /**
     * Devuelve true si el objeto de entrada implementa la interfaz indicada en el segundo argumento.
     *
     * @param $object
     * @param string $interfaceName
     * @return bool
     */
    protected function object_implements_interface($object, string $interfaceName): bool
    {
        $interfaces = class_implements($object);
        return isset($interfaces[$interfaceName]);
    }
}