<?php

namespace Beat\ColoredLogging\RecordMutators;

use Beat\ColoredLogging\Contracts\RecordMutator;

class InternalTrim implements RecordMutator
{
    public function mutateRecord(array $record): array
    {
        if (array_key_exists('message', $record)) {
            $record['message'] = $this->do_internal_trim($record['message']);
        }

        return $record;
    }

    protected function do_internal_trim(string $message): string
    {
        do {
            $new_formatted_record = str_replace(['[]', "\n", "\t", '\n'], ' ', $this->internal_trim($message));
            if ($new_formatted_record !== $message) {
                $message = $new_formatted_record;
            } else {
                break;
            }
        } while (true);

        return $message;
    }

    public function internal_trim(string $text): string
    {
        $clean_text = trim($text);

        do {
            $aux_text   = $clean_text;
            $clean_text = str_replace('  ', ' ', $clean_text);
        } while(strlen($aux_text) !== strlen($clean_text));

        return $clean_text;
    }
}